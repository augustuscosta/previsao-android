package br.com.m2msolutions.previsao.model;


public class Configuration{

	private Long id;
	
	private String key;
	
	private String value;
	
	private ApplicationConfiguration configuration;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public ApplicationConfiguration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(ApplicationConfiguration configuration) {
		this.configuration = configuration;
	}
	
	
}
