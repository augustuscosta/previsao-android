package br.com.m2msolutions.previsao.model;

import java.util.List;

public class Company{

	private Long id;
	
	private String name;
	
	private String tittle;
	
	private List<ApplicationConfiguration> configurations;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ApplicationConfiguration> getConfigurations() {
		return configurations;
	}

	public void setConfigurations(List<ApplicationConfiguration> configurations) {
		this.configurations = configurations;
	}

	public String getTittle() {
		return tittle;
	}

	public void setTittle(String tittle) {
		this.tittle = tittle;
	}
	
}
