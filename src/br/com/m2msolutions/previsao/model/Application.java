package br.com.m2msolutions.previsao.model;

import java.util.List;

public class Application{

	private Long id;
	
	private String name;
	
	private String identifier;
	
	private String url;

	private List<ApplicationConfiguration> configurations;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<ApplicationConfiguration> getConfigurations() {
		return configurations;
	}

	public void setConfigurations(List<ApplicationConfiguration> configurations) {
		this.configurations = configurations;
	}

}
