package br.com.m2msolutions.previsao.model;

public class ConfigurationMobile {
	
	private String companyTittle;
	private String url;
	private String help;
	
	public String getCompanyTittle() {
		return companyTittle;
	}
	public void setCompanyTittle(String companyTittle) {
		this.companyTittle = companyTittle;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getHelp() {
		return help;
	}
	public void setHelp(String help) {
		this.help = help;
	}
	
}
