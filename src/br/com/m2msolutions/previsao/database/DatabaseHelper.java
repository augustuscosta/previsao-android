package br.com.m2msolutions.previsao.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "m2mpaneldroid_applicationdata";

	private static final int DATABASE_VERSION = 1;
	
	private static final String DATABASE_CREATE = "create table configurations (_id integer primary key autoincrement,"
			+ "company_tittle text, " + "url text, " + "help text" + ");";

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		createTables(db);
	}

	private void createTables(SQLiteDatabase db) {
		db.beginTransaction();
		try {
			db.execSQL(DATABASE_CREATE);
			db.setTransactionSuccessful();
		} catch (final SQLException e) {
			Log.e("ERROR", "Error creating database. " + e);
		} finally {
			db.endTransaction();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {

	}

}
