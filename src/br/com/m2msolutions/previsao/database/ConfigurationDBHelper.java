package br.com.m2msolutions.previsao.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import br.com.m2msolutions.previsao.model.ApplicationConfiguration;
import br.com.m2msolutions.previsao.model.Configuration;
import br.com.m2msolutions.previsao.model.ConfigurationMobile;

public class ConfigurationDBHelper {

	// Database fields
	public static final String KEY_ROWID = "_id";
	public static final String KEY_COMPANY_TITTLE = "company_tittle";
	public static final String KEY_URL = "url";
	public static final String KEY_HELP = "help";
	private static final String DATABASE_TABLE = "configurations";
	private Context context;
	private SQLiteDatabase database;
	private DatabaseHelper dbHelper;
	
	public ConfigurationDBHelper(Context context) {
		this.context = context;
	}

	public ConfigurationDBHelper open() throws SQLException {
		dbHelper = new DatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
		return this;
	}

	public void close() {
		dbHelper.close();
	}

	public void storeConfigurations(List<ApplicationConfiguration> configurations) {
		if(configurations == null || configurations.size() == 0){
			return;
		}
		deleteAll();
		for(ApplicationConfiguration configuration : configurations){
			String tittle = null;
			String url = null;
			String help = null;
			if(configuration.getCompany() != null){
				tittle = configuration.getCompany().getTittle();
			}
			if(configuration.getConfigurations() != null){
				for(Configuration keyValue : configuration.getConfigurations()){
					if(keyValue.getKey().toUpperCase().equals("URL")){
						url = keyValue.getValue();
					}
					if(keyValue.getKey().toUpperCase().equals("HELP")){
						help = keyValue.getValue();
					}
				}
			}
			if(tittle != null && url != null){
				ContentValues initialValues = createContentValues(tittle, url, help);
				database.insert(DATABASE_TABLE, null, initialValues);
			}
		}
	}
	
	public List<ConfigurationMobile> getConfigurations(){
		return parseToBusService(fetchAll());
	}
	
	private Cursor fetchAll() {
		return database.query(DATABASE_TABLE, new String[] { KEY_ROWID,
				KEY_COMPANY_TITTLE, KEY_URL, KEY_HELP}, null, null, null,
				null, null);
	}
	

	private List<ConfigurationMobile> parseToBusService(Cursor cursor) {
		List<ConfigurationMobile> toReturn = null;
		if(cursor != null){
			toReturn = new ArrayList<ConfigurationMobile>();
			ConfigurationMobile configuration;
			if(cursor.moveToFirst()){
				while(cursor.isAfterLast() == false){
					configuration = new  ConfigurationMobile();
					configuration.setCompanyTittle(cursor.getString(1));
					configuration.setUrl(cursor.getString(2));
					configuration.setHelp(cursor.getString(3));
					toReturn.add(configuration);
					cursor.moveToNext();
				}
			}
			cursor.close();
		}
		return toReturn;
	}

	private ContentValues createContentValues(String companyTittle, String url,String help) {
		ContentValues values = new ContentValues();
		values.put(KEY_COMPANY_TITTLE, companyTittle);
		values.put(KEY_URL, url);
		values.put(KEY_HELP, help);
		return values;
	}
	
	public boolean deleteAll() {
		return database.delete(DATABASE_TABLE,null, null) > 0;
	}
}
