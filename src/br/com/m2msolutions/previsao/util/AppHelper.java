package br.com.m2msolutions.previsao.util;

import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.provider.Settings.Secure;

public class AppHelper extends Application {
	private static AppHelper instance = null;

	public static AppHelper getInstance() {
		if(instance != null) {
			return instance;
		} else {
			instance = new AppHelper();
			return instance;
		}
	}

	public void presentError(Context ctx, String title, String description) {
		AlertDialog.Builder d = new AlertDialog.Builder(ctx);
		d.setTitle(title);
		d.setMessage(description);
		d.setIcon(android.R.drawable.ic_dialog_alert);
		d.setNeutralButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
			}
		});
		d.show();
	}

	public String getUniqueDeviceID(Context context){
		return Secure.getString(context.getContentResolver(),
                Secure.ANDROID_ID); 
	}
	
	@Override
	public void onTerminate() {
		instance = null;
		super.onTerminate();
	}

}
