package br.com.m2msolutions.previsao.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import br.com.m2msolutions.previsao.R;

public class WebPanel extends Activity {

	final String server = "http://appbrt.m2mfrota.com/";
	//cooptrater.m2mfrota.com/

	private ProgressDialog progressDialog;
	private WebView webView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().requestFeature(Window.FEATURE_PROGRESS);
		getWindow().setFeatureInt(Window.FEATURE_PROGRESS,
				Window.PROGRESS_VISIBILITY_ON);
		createWebView();
		loadData();
		showSplashScreen();
	}

	private void showSplashScreen() {
		startActivity(new Intent(WebPanel.this, SplashScreen.class));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuItem refresh = menu.add(getString(R.string.refresh));
		refresh.setIcon(R.drawable.refresh);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.toString().equals(getString(R.string.refresh))) {
			loadData();
			return true;
		}

		return true;
	}

	private void createWebView() {
		webView = new WebView(this);
		webView.setBackgroundColor(Color.TRANSPARENT);
		webView.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.splashscreen));
		webView.getSettings().setJavaScriptEnabled(true);

		final Activity activity = this;
		webView.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {
				setTitle("Carregando...");
				setProgress(progress * 100);
				if (progress == 100)
					setTitle(R.string.app_name);
			}
		});

		
		webView.setWebViewClient(new WebViewClient() {
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				stopProgressDialog();
				webView.loadData("<html></html>", "text/html", null);
				loadData();
			}

			public void onPageFinished(WebView view, String url) {
	            stopProgressDialog();
	        }
			
		});
		setContentView(webView);
		
		
	}

	private void loadData() {
		startProgressDialog();
		webView.loadUrl(server);
	}
	
	private void startProgressDialog(){
		if (progressDialog == null) {
            //If no progress dialog, make one and set message
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Carregando");
            progressDialog.show();

            //Hide the webview while loading
            webView.setEnabled(false);
        } 
	}
	
	private void stopProgressDialog(){
		if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
            webView.setEnabled(true);
        }
	}

}