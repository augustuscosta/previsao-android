package br.com.m2msolutions.previsao.cloud;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import br.com.m2msolutions.previsao.model.ApplicationConfiguration;

import com.google.gson.Gson;

public class ConfigurationService extends RestClient {
	
	private final String ROOT_OBJECT = "list";

	public void getConfigurations(Context context) throws Exception{
		cleanParams();
		setUrl(CONFIGURATION_URL);
		addParam("identifier", APP_IDENTIFIER);
		execute(RequestMethod.GET);
	}
	
	public List<ApplicationConfiguration> getConfigurationsFromResponse() throws JSONException, ParseException{
		JSONArray jsonArray = getJsonObjectArrayFromResponse(ROOT_OBJECT);
		if(jsonArray != null){
			Gson gson = new Gson();
			List<ApplicationConfiguration> toReturn = new ArrayList<ApplicationConfiguration>();
			ApplicationConfiguration configuration;
			for (int i = 0; i < jsonArray.length(); i++) {
				configuration = new ApplicationConfiguration();
				configuration = gson.fromJson(jsonArray.getJSONObject(i).toString(), ApplicationConfiguration.class);
				toReturn.add(configuration);
			}
			return toReturn;
		}
		return null;
	}
}

